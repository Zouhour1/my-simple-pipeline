*** Settings ***
Suite Teardown    Close All Browsers
Library           Selenium2Library

*** Variables ***
${URL}            https://messageriepro3.orange.fr
${Browser}        Chrome
${login-destinataire-01}    test.nonreglight@orange.fr
${password-destinataire-01}    Password1
${locator-text-login}    [id="login"]
${locator-text-mot_de_passe}    [id="password"]
${locator-bouton-s_identifier}    .eui-btn-sub
${locator-bouton-s_continuer}    .eui-btn-sub

*** Test Cases ***
Authentification et actualisation de la page
    [Documentation]    Authetification et actualisation de la page
    ouvrir l application    ${Browser}    ${URL}    ${login-destinataire-01}    ${password-destinataire-01}
    actualiser la page

*** Keywords ***
ouvrir l application
    [Arguments]    ${Browser}    ${URL}    ${adresse-mail}    ${mot-de-passe}
    Open Browser    ${URL}    ${Browser}
    Maximize Browser Window
    Input text    css=${locator-text-login}    ${login-destinataire-01}
    Click Element    css=${locator-bouton-s_continuer}
    Wait Until Element Is Visible    css=${locator-text-mot_de_passe}
    Input text    css=${locator-text-mot_de_passe}    ${password-destinataire-01}
    Click Element    css=${locator-bouton-s_identifier}
    Sleep    10s
    Reload Page

actualiser la page
    Sleep    10s
    Reload Page
